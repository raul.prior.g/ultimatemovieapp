Aplicación de ejemplo con estructura FragmentBottom, live data, funciones de extension, MVVM, retrofit, inyección de dependencias y room  


/*
Ejemplo de request
https://api.themoviedb.org/3/movie/popular?api_key=<Apikey>&language=en-US&page=1
https://www.themoviedb.org/talk/5f3ef4eec175b200365ee352

https://api.themoviedb.org/3/tv/popular?api_key=<Apikey>&language=en-US&page=1
//Generos
https://api.themoviedb.org/3/genre/movie/list?api_key=<Apikey>&language=en-US
//Peliculas por Genero
https://api.themoviedb.org/3/discover/movie?api_key=<Apikey>&sort_by=popularity.desc&page=1&with_genres=53


//Color
https://www.colr.org/json/colors/random/7
https://www.colr.org/json/colors/random/7?tag=cheese
*/

----> Añadir el permiso de internet
{
    <uses-permission android:name="android.permission.INTERNET"/>
}


----> Implementar las dependencias necesarias
{
    // Fragment
    implementation "androidx.fragment:fragment-ktx:1.3.2"
    // Activity
    implementation "androidx.activity:activity-ktx:1.2.2"
    // ViewModel
    implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:2.3.1"
    // LiveData
    implementation "androidx.lifecycle:lifecycle-livedata-ktx:2.3.1"
    // Retrofit
    implementation "com.squareup.retrofit2:retrofit:2.9.0"
    implementation "com.squareup.retrofit2:converter-gson:2.9.0"
    //Corrutinas
    implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-android:1.3.6'
    //SerializedName
    implementation 'com.google.code.gson:gson:2.8.8'
}

----> Agregar el viewBinding 
{
   buildFeatures
   {
      viewBinding = true
   }
}

----> Crear el arbol de directorios
{
    core
    data
        model
        network
    domain
    ui
        view
        modelview
}

----> Crear mis modelos de datos en base a los jsons en la carpeta data/model

----> Generar mi RetrofiHelper en el directorio core

----> Generar la clase QueryInterceptor en la carpeta core

----> Generar la interfaz MoviesApiClient en data/network

----> Generar la clase MovieService en la carpeta data/network

----> Generar la clase MovieProvider en la carpeta data/model

----> Genear la Clase MovieRepository en la carpeta data data

----> Generar la claes GetMovieUseCase en la carpeta domain

----> Generar la clase MovieViewModel en ui/viewModel

----> Generar el xml item_movie

----> Generar la clase MovieAdapter en ui/view

/*
    Flujo de Trabajo
    GetMoviesUseCase (Llama) -> MovieRepository (Llama) -> MoviesService (Llama)-> RetrofitHelper(Utilizar) -> MovieApiClient (Pregunta)-> Servidor
                                MovieRepository (Guarda en) -> MovieProvider
*/

***********************************************************
Inyeccion de dependencias
***********************************************************
----> Implementacion de depencencias en el gradle General
{
buildscript {
ext.kotlin_version = "1.6.10"
ext.hilt_version = '2.42'
repositories {
google()
mavenCentral()
}
dependencies {
classpath "com.android.tools.build:gradle:4.1.3"
classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"
classpath "com.google.dagger:hilt-android-gradle-plugin:$hilt_version"
}
}
}

----> Implementacion de depencencias en el gradle de app{
    plugins {
    id 'com.android.application'--Esta no
    id 'org.jetbrains.kotlin.android'--Esta no
    id 'kotlin-kapt'
    id 'dagger.hilt.android.plugin'
    }

    dependencies{
        //Dager-hilt
        implementation "com.google.dagger:hilt-android:$hilt_version"
        kapt "com.google.dagger:hilt-android-compiler:$hilt_version"
    }
}

----> Generar la clase MovieAppAplication en la raiz del proyecto

----> En el manifest agregamos la siguiente linea en la rama application
android:name=".MvvmExampleApp"

----> Injectas clases

----> Crear la carpeta di en el directorio raiz

--->Generar la el objeto  NetworkModuleSingelton

***********************************************************
Room
***********************************************************

--->Agregar las dependencias

//Room
implementation "androidx.room:room-ktx:2.4.0"
kapt "androidx.room:room-compiler:2.4.0"

--->Crear un directorio database en data

--->Crear un directorio entities en data/database

--->Crear las dataclass de entities en el directorio data/database/entities

--->Crear un directorio dao en data/database

--->Crear la interfaz MovieDao en data/database

--->Crear el objeto RoomModule en di

--->Inyectar el Dao en los services

--->Crear el directorio model en domain

--->Crear los items en domain/model

--->Generar mappers