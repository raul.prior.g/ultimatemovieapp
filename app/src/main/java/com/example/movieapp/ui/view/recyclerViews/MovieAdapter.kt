package com.example.movieapp.ui.view.recyclerViews

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp.R
import com.example.movieapp.data.model.MovieModel
import com.example.movieapp.domain.GetDetailsMovieUseCase
import com.example.movieapp.domain.model.MovieItem
import com.example.movieapp.ui.viewModel.DetailsMovieViewModel
import javax.inject.Inject

class MovieAdapter @Inject constructor (
    val movies: List<MovieItem>
    ,private val detailsMovieViewModel: DetailsMovieViewModel
    ): RecyclerView.Adapter<MovieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val layoutInflater : LayoutInflater = LayoutInflater.from(parent.context)
        return MovieViewHolder(layoutInflater.inflate(R.layout.item_movie, parent, false), detailsMovieViewModel)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val item = movies[position]

        holder.bind(item)
    }

    override fun getItemCount(): Int = movies.size
}