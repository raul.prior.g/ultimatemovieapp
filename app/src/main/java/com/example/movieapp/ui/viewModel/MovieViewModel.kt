package com.example.movieapp.ui.viewModel

import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.movieapp.domain.GetDetailsMovieUseCase
import com.example.movieapp.domain.GetMoviesByGenreUseCase
import com.example.movieapp.domain.GetMoviesUseCase
import com.example.movieapp.domain.model.MovieItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieViewModel @Inject constructor(
    private var getMoviesUseCase: GetMoviesUseCase
    ,private var getMoviesByGenreUseCase: GetMoviesByGenreUseCase
    ,private var getDetailsMovieUseCase: GetDetailsMovieUseCase
) : ViewModel() {

    var movieModel = MutableLiveData<List<MovieItem>>()
    var snRefresing = MutableLiveData<Boolean>()

    fun onCreate(query:String){
        viewModelScope.launch {
            val movieList: List<MovieItem> = getMoviesUseCase.checkFromBDFirst(query)

            if(movieList.isNotEmpty()){
                movieModel.postValue(movieList)
            }

            movieList.map {
                getDetailsMovieUseCase.getDetails(it.intIdentity)
            }
        }
    }

    fun getMovieByGenre(intIdGenre: Int) {
        viewModelScope.launch{
            val movieList = getMoviesByGenreUseCase.invoke(intIdGenre)

            if(movieList.isNotEmpty()){
                movieModel.postValue(movieList)
                movieList.map {
                    getDetailsMovieUseCase.getDetails(it.intIdentity)
                }
            }
        }
    }

    fun onRefresh(query:String){
        viewModelScope.launch {
            snRefresing.postValue(true);

            val movieList: List<MovieItem> = getMoviesUseCase.checkFromApiFirst(query)

            if(movieList.isNotEmpty()){
                movieModel.postValue(movieList)
            }

            movieList.map {
                getDetailsMovieUseCase.getDetails(it.intIdentity)
            }

            snRefresing.postValue(false);
        }
    }

}