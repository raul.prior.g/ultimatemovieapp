package com.example.movieapp.ui.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.movieapp.domain.GetDetailsMovieUseCase
import com.example.movieapp.domain.model.DetailsMovieItem
import com.example.movieapp.domain.model.GenreItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailsMovieViewModel @Inject constructor(
    private val getDetailsMovieUseCase: GetDetailsMovieUseCase
): ViewModel() {

    var detailsModel = MutableLiveData<DetailsMovieItem>()
    var genresModel = MutableLiveData<List<GenreItem>>()

    fun onCreate(idMovie: Int){
        viewModelScope.launch {
            var details = getDetailsMovieUseCase.getDetails(idMovie)
            val nullDetails = DetailsMovieItem(0, "", "", "", "", "", "No Internet Conection", "")

            if(details == null)
                details = nullDetails

            detailsModel.postValue(details)

            val genresList = getDetailsMovieUseCase.getMovieGenres(idMovie)
            genresModel.postValue(genresList)


        }
    }
}