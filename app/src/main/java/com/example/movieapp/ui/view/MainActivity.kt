package com.example.movieapp.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.movieapp.data.model.DetailsMovieModel
import com.example.movieapp.databinding.ActivityMainBinding
import com.example.movieapp.domain.GetDetailsMovieUseCase
import com.example.movieapp.ui.view.recyclerViews.MovieAdapter
import com.example.movieapp.ui.view.recyclerViews.GenresAdapter
import com.example.movieapp.ui.viewModel.DetailsMovieViewModel
import com.example.movieapp.ui.viewModel.GenresViewModel
import com.example.movieapp.ui.viewModel.MovieViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding:ActivityMainBinding

    private val movieViewModel: MovieViewModel by viewModels()
    private val genresViewModel: GenresViewModel by viewModels()
    private val detailsMovieViewModel: DetailsMovieViewModel by viewModels()

    private lateinit var movieAdapter: MovieAdapter
    private lateinit var genresAdapter: GenresAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        genresViewModel.onCreate()

        genresViewModel.movieGenresModel.observe(this, Observer { movieGenres->
            genresAdapter = GenresAdapter(movieGenres, movieViewModel)
            binding.rvMovieGenres.layoutManager = LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL,
                false)
            binding.rvMovieGenres.adapter = genresAdapter
        })

        movieViewModel.onCreate("popular")

        movieViewModel.movieModel.observe(this, Observer{ movies ->
            movieAdapter = MovieAdapter(movies, detailsMovieViewModel)
            binding.rvMovies.layoutManager = LinearLayoutManager(this)
            binding.rvMovies.adapter = movieAdapter
        })

        movieViewModel.snRefresing.observe(this, {
            binding.swiperMovies.isRefreshing = it
        })

        binding.swiperMovies.setOnRefreshListener {
            movieViewModel.onRefresh("popular")
            genresViewModel.onCreate()
        }

    }
}