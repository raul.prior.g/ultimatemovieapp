package com.example.movieapp.ui.view

import android.graphics.Color
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.movieapp.databinding.BottomsheetDetailMovieFragmentBinding
import com.example.movieapp.ui.viewModel.DetailsMovieViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BottomSheetDetailsMovieFragment (
        val detailsMovieViewModel: DetailsMovieViewModel
    ) : BottomSheetDialogFragment() {

    private var _binding: BottomsheetDetailMovieFragmentBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = BottomsheetDetailMovieFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        detailsMovieViewModel.detailsModel.observe(this,{
            binding.tvTitle.text = it.title
            binding.tvHomePage.text = it.homepage
            binding.tvOriginalTitle.text = "Original title \n" + it.original_title
            binding.tvReleaseDate.text  = "Release date \n" + it.release_date
            binding.tvStatus.text = "Status \n" + it.status
            binding.tvOverview.text = "Overview \n" + it.overview

            Picasso.get()
                .load("https://image.tmdb.org/t/p/original/" + it.poster_path)
                .into(binding.ivPoster)

            binding.tvHomePage.setMovementMethod(LinkMovementMethod.getInstance())
            binding.tvHomePage.setLinkTextColor(Color.BLUE)
        })

        detailsMovieViewModel.genresModel.observe(this, { genresList ->
            var genresText = "Genres \n"
            genresList.map {
                 genresText += it.genre + "/"
            }

            binding.tvGenres.text = genresText.substring(0, genresText.length - 1)
        })

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}