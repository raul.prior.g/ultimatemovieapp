package com.example.movieapp.ui.view.recyclerViews

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp.R
import com.example.movieapp.domain.model.GenreItem
import com.example.movieapp.ui.viewModel.MovieViewModel

class GenresAdapter(val movieGenre: List<GenreItem>, val movieViewModel: MovieViewModel):
    RecyclerView.Adapter<GenresViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenresViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        return GenresViewHolder(layoutInflater.inflate(R.layout.item_movie_genres, parent, false))
    }

    override fun onBindViewHolder(holder: GenresViewHolder, position: Int) {
        val item = movieGenre[position]
        holder.bind(item, movieViewModel)
    }

    override fun getItemCount(): Int = movieGenre.size
}