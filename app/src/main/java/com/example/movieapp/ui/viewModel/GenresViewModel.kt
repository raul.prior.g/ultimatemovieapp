package com.example.movieapp.ui.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.movieapp.domain.ColorsUseCase
import com.example.movieapp.domain.GetGenreUseCase
import com.example.movieapp.domain.model.GenreItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GenresViewModel @Inject constructor(
    private var getMoviesUseCase: GetGenreUseCase,
): ViewModel() {

    var movieGenresModel = MutableLiveData<List<GenreItem>>()

    fun onCreate(){
        viewModelScope.launch{
            val genres = getMoviesUseCase.invoke()
            movieGenresModel.postValue(genres)
        }
    }
}