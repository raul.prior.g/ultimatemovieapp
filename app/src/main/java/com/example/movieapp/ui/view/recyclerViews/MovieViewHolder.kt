package com.example.movieapp.ui.view.recyclerViews

import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp.databinding.ItemMovieBinding
import com.example.movieapp.domain.model.MovieItem
import com.example.movieapp.ui.view.BottomSheetDetailsMovieFragment
import com.example.movieapp.ui.viewModel.DetailsMovieViewModel
import com.squareup.picasso.Picasso
import javax.inject.Inject

class MovieViewHolder @Inject constructor (
    view: View
    ,private val detailsMovieViewModel: DetailsMovieViewModel
): RecyclerView.ViewHolder(view) {

    private val binding = ItemMovieBinding.bind(view)

    fun bind(movie: MovieItem){
        binding.tvOriginalTitle.text = "Original Title:\n" + movie.strOriginalTitle
        binding.tvRelaseDate.text = "Relase Date:\n" + movie.strReleaseDate
        binding.tvOriginalLanguage.text = "Original Language:\n" +  movie.strOriginalLanguage
        binding.tvPopularity.text = "Popularity:\n" + movie.dbPopularity.toString()
        Picasso.get()
            .load("https://image.tmdb.org/t/p/original/" + movie.strPosterPath)
            .into(binding.ivPoster)

        binding.cvMovies.setOnClickListener(View.OnClickListener {
            detailsMovieViewModel.onCreate(movie.intIdentity)
            val bottomSheetDetailsMovieFragment = BottomSheetDetailsMovieFragment(detailsMovieViewModel)

            bottomSheetDetailsMovieFragment.show((binding.root.context as FragmentActivity).supportFragmentManager, "bottomSheetDetailsMovieFragment")
        })
    }
}