package com.example.movieapp.ui.view.recyclerViews

import android.graphics.Color
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp.core.ColorTex
import com.example.movieapp.data.model.GenreModel
import com.example.movieapp.databinding.ItemMovieGenresBinding
import com.example.movieapp.domain.model.GenreItem
import com.example.movieapp.ui.viewModel.MovieViewModel

class GenresViewHolder(view: View): RecyclerView.ViewHolder(view) {

    private val binding = ItemMovieGenresBinding.bind(view)

    fun bind(genre: GenreItem, movieViewModel: MovieViewModel){

        binding.tvGenre.text = genre.genre

        if(!genre.backgroundColor.isEmpty()){
            binding.clGenresLayout.setBackgroundColor(Color.parseColor("#" + genre.backgroundColor))
            binding.tvGenre.setTextColor(Color.parseColor("#" + genre.backgroundColor).ColorTex())
        }

        binding.cvGenre.setOnClickListener(View.OnClickListener {
            movieViewModel.getMovieByGenre(genre.id)
        })


    }
}