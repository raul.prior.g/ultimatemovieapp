package com.example.movieapp.core

import android.graphics.Color
import androidx.core.graphics.ColorUtils

fun Int.ColorTex(): Int {

    if(ColorUtils.calculateLuminance(this) < 0.5)
        return Color.WHITE
    else
        return Color.BLACK
}