package com.example.movieapp.core

import dagger.hilt.InstallIn
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class QueryDiscoveryInterceptor @Inject constructor() : Interceptor {

    var intIdGenre: Int = 0

    override fun intercept(chain: Interceptor.Chain): Response {
        val urlParametres = chain.request().url().newBuilder()
            .addQueryParameter("api_key", "ab054b4d855dd261858f2a9fe1ecdc27")
            .addQueryParameter("sort_by", "popularity.desc")
            .addQueryParameter("page", "1")
            .addQueryParameter("with_genres", "" + intIdGenre)
            .build()

        val request = chain.request().newBuilder()
            .url(urlParametres)
            .build()

        return chain.proceed(request)
    }
}
