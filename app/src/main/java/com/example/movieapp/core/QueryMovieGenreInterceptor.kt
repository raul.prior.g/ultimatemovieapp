package com.example.movieapp.core

import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class QueryMovieGenreInterceptor @Inject constructor(): Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val urlQuery = chain.request().url().newBuilder()
            .addQueryParameter("api_key", "ab054b4d855dd261858f2a9fe1ecdc27")
            .addQueryParameter("language", "en-US")
            .build()

        val request = chain.request()
            .newBuilder()
            .url(urlQuery)
            .build()

        return chain.proceed(request)
    }
}