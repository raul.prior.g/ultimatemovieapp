package com.example.movieapp.di

import com.example.movieapp.core.QueryDiscoveryInterceptor
import com.example.movieapp.core.QueryMovieGenreInterceptor
import com.example.movieapp.core.QueryMovieInterceptor
import com.example.movieapp.data.network.ColorApiClient
import com.example.movieapp.data.network.GenresApiClient
import com.example.movieapp.data.network.MoviesApiClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Qualifier
import javax.inject.Singleton

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class MovieByGenreApiClient

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class MovieApiClient

@Module
@InstallIn(SingletonComponent::class)
object NetworkModuleSingelton {

    @MovieApiClient
    @Singleton
    @Provides
    fun provideMovieApiClient(queryMovieInterceptor: QueryMovieInterceptor): MoviesApiClient{
        val queryMovieClient = OkHttpClient.Builder()
            .addInterceptor(queryMovieInterceptor)
            .build()

        return Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/movie/")
            .client(queryMovieClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(MoviesApiClient::class.java)
    }

    @MovieByGenreApiClient
    @Provides
    fun provideMovieByGenreApiClient(queryDiscoveryInterceptor: QueryDiscoveryInterceptor): MoviesApiClient {
        val client = OkHttpClient.Builder()
            .addInterceptor(queryDiscoveryInterceptor)
            .build()

        return Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/discover/")
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(MoviesApiClient::class.java)
    }

    @Provides
    fun provideRetrofiGenres(queryMovieGenreInterceptor: QueryMovieGenreInterceptor): GenresApiClient{

        val queryMovieGenreClient = OkHttpClient.Builder()
            .addInterceptor(queryMovieGenreInterceptor)
            .build()

        return  Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/genre/")
            .client(queryMovieGenreClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(GenresApiClient::class.java)
    }

    @Provides
    fun provideColorsApiClient(): ColorApiClient{
        return Retrofit.Builder()
            .baseUrl("https://www.colr.org/json/colors/random/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ColorApiClient::class.java)
    }
}



