package com.example.movieapp.di

import android.content.Context
import androidx.room.Room
import com.example.movieapp.data.database.DataBaseApp
import com.example.movieapp.data.database.dao.DetailsMovieDao
import com.example.movieapp.data.database.dao.GenreDao
import com.example.movieapp.data.database.dao.MovieDao
import com.example.movieapp.data.database.dao.RelMovieGenreDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    private val MOVIE_DATABASE_NAME = "movie_app_db"

    @Singleton
    @Provides
    fun provideRoom(@ApplicationContext context: Context): DataBaseApp {
        return Room.databaseBuilder(context, DataBaseApp::class.java, MOVIE_DATABASE_NAME).build()
    }

    @Singleton
    @Provides
    fun provideMovieDao(db: DataBaseApp): MovieDao {
        return  db.getMovieDao()
    }

    @Singleton
    @Provides
    fun provideGenreDao(db: DataBaseApp): GenreDao {
        return  db.getGenreDao()
    }

    @Singleton
    @Provides
    fun provideDetailsMovieDao(db: DataBaseApp): DetailsMovieDao {
        return db.getDetailDao()
    }

    @Singleton
    @Provides
    fun provideRelMovieGenreDao(db: DataBaseApp): RelMovieGenreDao {
        return db.getRelMovieGenreDao()
    }
}