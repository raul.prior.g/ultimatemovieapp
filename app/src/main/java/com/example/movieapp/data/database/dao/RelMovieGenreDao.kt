package com.example.movieapp.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.movieapp.data.database.entities.GenreEntity
import com.example.movieapp.data.database.entities.RelMovieGenresEntity

@Dao
interface RelMovieGenreDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRelMovieGenre(relMovieGenre: RelMovieGenresEntity)

    @Query("Select g.* " +
            "from rel_movie_genres rm " +
            "Inner Join cat_genres g on rm.id_genre = g.id " +
            "where rm.id_movie = :idMovie")
    suspend fun getRelMovieGenre(idMovie: Int): List<GenreEntity>?
}