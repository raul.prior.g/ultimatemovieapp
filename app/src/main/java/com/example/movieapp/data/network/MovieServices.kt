package com.example.movieapp.data.network

import android.util.Log
import com.example.movieapp.core.QueryDiscoveryInterceptor
import com.example.movieapp.data.model.DetailsMovieModel
import com.example.movieapp.data.model.MovieModel
import com.example.movieapp.di.MovieApiClient
import com.example.movieapp.di.MovieByGenreApiClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MovieServices @Inject constructor(
    @MovieApiClient private val apiMovie: MoviesApiClient,
    @MovieByGenreApiClient private val apiMovieByGenre: MoviesApiClient,
    val queryDiscoveryInterceptor: QueryDiscoveryInterceptor
) {

    suspend fun getMoviesOrderBy(query: String): List<MovieModel>{
        return withContext(Dispatchers.IO){
            try {
                val response = apiMovie.getMoviesOrderBy(query)

                response.body()?.movies ?: emptyList()
            }catch (e: Exception){
                Log.e("TAG", "getMoviesOrderBy: " + e.message)
                emptyList()
            }
        }
    }

    suspend fun getMoviesByGenre(intIdGenre: Int): List<MovieModel>{
        return withContext(Dispatchers.IO){
            try {
                queryDiscoveryInterceptor.intIdGenre = intIdGenre
                val response = apiMovieByGenre.getMoviesByGenre()

                response.body()?.movies ?: emptyList()
            }catch (e: Exception){
                Log.e("TAG", "getMoviesByGenre: " + e.message)
                emptyList()
            }
        }
    }

    suspend fun getDetailsMovie(idMovie: Int): DetailsMovieModel?{
        return withContext(Dispatchers.IO){
            try{
                val response = apiMovie.getDetailsMovie("" + idMovie)

                response.body()
            }catch (ex: Exception){
                Log.e("TAG", "getDetailMovie: " + ex.message)
                null
            }
        }
    }

}