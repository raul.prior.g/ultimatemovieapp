package com.example.movieapp.data.model

import com.google.gson.annotations.SerializedName
import javax.inject.Inject

data class DetailsMovieModel @Inject constructor (
            @SerializedName("id") val id: Int
            ,@SerializedName("genres") val genres: List<GenreModel>
            ,@SerializedName("homepage") val homepage: String
            ,@SerializedName("overview") val overview: String
            ,@SerializedName("original_title") val original_title: String
            ,@SerializedName("release_date") val release_date: String
            ,@SerializedName("status") val status: String
            ,@SerializedName("title") val title: String
            ,@SerializedName("poster_path") val poster_path: String
        )
