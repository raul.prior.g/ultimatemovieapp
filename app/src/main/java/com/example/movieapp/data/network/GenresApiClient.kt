package com.example.movieapp.data.network

import com.example.movieapp.data.model.GenresChatcherModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface GenresApiClient {
    @GET
    suspend fun getMovieGenre(@Url strTipo:String): Response<GenresChatcherModel>
}