package com.example.movieapp.data

import com.example.movieapp.data.network.ColorsService
import javax.inject.Inject

class ColorsRepository @Inject constructor(
  private val service: ColorsService
){
    suspend fun getRandomColors(intNoColors: Int): List<String>{
        val colors = service.getColors(intNoColors)

        return colors
    }
}