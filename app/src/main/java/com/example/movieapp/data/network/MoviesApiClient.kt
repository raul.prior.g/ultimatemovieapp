package com.example.movieapp.data.network

import com.example.movieapp.data.model.DetailsMovieModel
import com.example.movieapp.data.model.MoviesCatcherModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface MoviesApiClient {
    @GET
    suspend fun getMoviesOrderBy(@Url url:String): Response<MoviesCatcherModel>

    @GET("movie")
    suspend fun getMoviesByGenre(): Response<MoviesCatcherModel>

    @GET
    suspend fun getDetailsMovie(@Url idMovie:String): Response<DetailsMovieModel>
}