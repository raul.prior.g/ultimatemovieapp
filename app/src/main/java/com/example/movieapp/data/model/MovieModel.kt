package com.example.movieapp.data.model

import com.google.gson.annotations.SerializedName
import javax.inject.Inject

data class MoviesCatcherModel @Inject constructor (
    @SerializedName("page") var intPage: Int
    ,@SerializedName("results") var movies: List<MovieModel>
    ,@SerializedName("total_pages") var intTotalPages: Int
    ,@SerializedName("total_results") var intTotalResults: Int
    ,@SerializedName("success") var snSuccess: Boolean ?
    ,@SerializedName("status_message") var strStatusMessage: String ?
    ,@SerializedName("status_code") var intStatusCode: Int ?
)

data class MovieModel @Inject constructor (
    @SerializedName("id") var intIdentity: Int
    ,@SerializedName("original_language") var strOriginalLanguage: String
    ,@SerializedName("original_title") var strOriginalTitle: String
    ,@SerializedName("popularity") var dbPopularity: Double
    ,@SerializedName("poster_path") var strPosterPath: String
    ,@SerializedName("release_date") var strReleaseDate: String
    ,@SerializedName("title") var strTitle: String
    ,@SerializedName("vote_average") var dbVoteAverage: Double
    ,@SerializedName("vote_count") var dbVoteCount: Double
)

