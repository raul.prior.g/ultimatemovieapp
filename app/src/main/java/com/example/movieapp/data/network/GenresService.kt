package com.example.movieapp.data.network

import android.util.Log
import com.example.movieapp.data.model.GenreModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GenresService @Inject constructor(
    private val api: GenresApiClient
) {
    suspend fun getAllMovieGenres(): List<GenreModel>{
        return withContext(Dispatchers.IO){
            try{
                val response = api.getMovieGenre("movie/list").body()?.genres
                response ?: emptyList()
            }catch (ex: Exception){
                Log.e("TAG", "getAllMovieGenres: " + ex.message )
                emptyList()
            }
        }
    }
}