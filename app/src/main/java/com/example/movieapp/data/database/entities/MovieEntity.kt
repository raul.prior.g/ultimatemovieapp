package com.example.movieapp.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.movieapp.domain.model.MovieItem

@Entity(tableName = "ctrl_movies")
data class MovieEntity (
    @PrimaryKey(autoGenerate = false) @ColumnInfo(name = "id_from_api") var intIdentity: Int
    ,@ColumnInfo(name = "original_language") var strOriginalLanguage: String
    ,@ColumnInfo(name = "original_title") var strOriginalTitle: String
    ,@ColumnInfo(name = "popularity") var dbPopularity: Double
    ,@ColumnInfo(name = "poster_path") var strPosterPath: String
    ,@ColumnInfo(name = "release_date") var strReleaseDate: String
    ,@ColumnInfo(name = "title") var strTitle: String
    ,@ColumnInfo(name = "vote_average") var dbVoteAverage: Double
    ,@ColumnInfo(name = "vote_count") var dbVoteCount: Double
)

fun MovieItem.toDataBase() = MovieEntity(
    intIdentity = intIdentity
    ,strOriginalLanguage = strOriginalLanguage
    , strOriginalTitle =  strOriginalTitle
    , dbPopularity = dbPopularity
    , strPosterPath = strPosterPath
    , strReleaseDate = strReleaseDate
    , strTitle = strTitle
    , dbVoteAverage = dbVoteAverage
    , dbVoteCount = dbVoteCount
)