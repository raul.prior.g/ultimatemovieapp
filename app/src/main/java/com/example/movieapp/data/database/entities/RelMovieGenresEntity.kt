package com.example.movieapp.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "rel_movie_genres")
data class RelMovieGenresEntity (
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id_rel") val id_rel: Int = 0
    ,@ColumnInfo(name = "id_movie") val id_movie: Int
    ,@ColumnInfo(name = "id_genre") val id_genre: Int
    )