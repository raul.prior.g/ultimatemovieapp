package com.example.movieapp.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.movieapp.data.database.entities.DetailsMovieEntity

@Dao
interface DetailsMovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDetailsMovie(detailsMovieEntity: DetailsMovieEntity)

    @Query("Select * From ctrl_details_movie where movie_id = :idMovie")
    suspend fun getDetailsFromMovie(idMovie: Int): DetailsMovieEntity?

    @Query("Select * From ctrl_details_movie")
    suspend fun getAllDetails(): List<DetailsMovieEntity>

}