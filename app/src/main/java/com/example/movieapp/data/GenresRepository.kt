package com.example.movieapp.data

import com.example.movieapp.data.database.dao.GenreDao
import com.example.movieapp.data.database.entities.GenreEntity
import com.example.movieapp.data.model.GenreModel
import com.example.movieapp.data.network.GenresService
import com.example.movieapp.domain.model.GenreItem
import com.example.movieapp.domain.model.toDomain
import javax.inject.Inject

class GenresRepository @Inject constructor(
    private val service: GenresService
    ,private val genreDao: GenreDao
) {

    suspend fun getAllGenresByApi() : List<GenreItem>{
        val genres = service.getAllMovieGenres()
        return genres.map { it.toDomain() }
    }

    suspend fun getAllGenresByDB(): List<GenreItem>{
        val genres = genreDao.getAllGenres()
        return genres.map { it.toDomain() }
    }

    suspend fun insertGenres(genres: List<GenreEntity>){
        genreDao.insertGenres(genres)
    }

    suspend fun deleteAllGenres(){
        genreDao.deleteAllGenres()
    }
}