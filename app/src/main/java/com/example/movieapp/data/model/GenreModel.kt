package com.example.movieapp.data.model

import com.google.gson.annotations.SerializedName
import javax.inject.Inject

data class GenresChatcherModel @Inject constructor (
    @SerializedName("genres")val genres: List<GenreModel>
)

data class GenreModel @Inject constructor (
    @SerializedName("id") val id: Int
    ,@SerializedName("name") val genre: String
    ,@SerializedName("backgroundColor") var backgroundColor: String?
    ,@SerializedName("type") var type: String?
)

