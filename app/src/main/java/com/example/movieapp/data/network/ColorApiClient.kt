package com.example.movieapp.data.network

import com.example.movieapp.data.model.ColorModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface ColorApiClient {
    @GET
    suspend fun getColors(@Url strNoColors:String):Response<ColorModel>
}