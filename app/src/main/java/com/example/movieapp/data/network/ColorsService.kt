package com.example.movieapp.data.network

import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ColorsService @Inject constructor(
    private val getColorApiClient: ColorApiClient
) {
    suspend fun getColors(intNoColors: Int): List<String>{
        return withContext(Dispatchers.IO){
            try {
                val response = getColorApiClient.getColors("" + intNoColors)
                    .body()
                    ?.colorHex

                response ?: emptyList()
            }catch (e: Exception){
                Log.e("TAG", "getColors: " + e.message)
                emptyList()
            }
        }
    }
}