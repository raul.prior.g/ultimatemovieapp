package com.example.movieapp.data.model

import com.google.gson.annotations.SerializedName

data class ColorModel (
    @SerializedName("matching_colors") var colorHex:List<String>
)