package com.example.movieapp.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.movieapp.data.database.entities.MovieEntity
import com.example.movieapp.data.model.MovieModel
import com.example.movieapp.domain.model.MovieItem

@Dao
interface MovieDao {

    @Query("Select * From ctrl_movies order by popularity desc")
    suspend fun getAllMovies(): List<MovieEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllMovies(movies: List<MovieEntity>)

    @Query("Delete from ctrl_movies")
    suspend fun deleteMovies()

    @Query("Delete from rel_movie_genres")
    suspend fun deleteRelMovieGenre()

    @Query("Delete from ctrl_details_movie")
    suspend fun deleteDetails()

    @Query("Select distinct m.* " +
            "From ctrl_movies m " +
            "Inner Join rel_movie_genres rmg On m.id_from_api = rmg.id_movie " +
            "Inner Join cat_genres g on rmg.id_genre = g.id " +
            "Where g.id = :idGenre"
    )
    suspend fun getMoviesByGenre(idGenre: Int):List<MovieEntity>?
}