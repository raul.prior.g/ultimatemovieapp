package com.example.movieapp.data

import android.util.Log
import com.example.movieapp.data.database.dao.DetailsMovieDao
import com.example.movieapp.data.database.dao.MovieDao
import com.example.movieapp.data.database.dao.RelMovieGenreDao
import com.example.movieapp.data.database.entities.DetailsMovieEntity
import com.example.movieapp.data.database.entities.GenreEntity
import com.example.movieapp.data.database.entities.MovieEntity
import com.example.movieapp.data.database.entities.RelMovieGenresEntity
import com.example.movieapp.data.model.DetailsMovieModel
import com.example.movieapp.data.model.MovieModel
import com.example.movieapp.data.network.MovieServices
import com.example.movieapp.domain.model.MovieItem
import com.example.movieapp.domain.model.toDomain
import javax.inject.Inject

class MoviesRepository @Inject constructor(
    private val service: MovieServices
    ,private val movieDao: MovieDao
    ,private val detailsMovieDao: DetailsMovieDao
    ,private val relMovieGenreDao: RelMovieGenreDao
    ) {

    suspend fun getAllMoviesFromApi(query:String):List<MovieItem>{
        val movieModelList = service.getMoviesOrderBy(query)

        val movieItem =  movieModelList.map { it.toDomain() }

        return movieItem
    }

    suspend fun getAllMoviesFromDataBase(): List<MovieItem>{
        val movieEntity = movieDao.getAllMovies()

        val movieItem = movieEntity.map { it.toDomain() }

        return movieItem
    }

    suspend fun insertMovies(movies: List<MovieEntity>) {
        Log.e("TAG", "insertMovies: " + movies.toString())
        movieDao.insertAllMovies(movies)
    }

    suspend fun deleteAllMovies() {
        movieDao.deleteRelMovieGenre()
        movieDao.deleteDetails()
        movieDao.deleteMovies()
    }

    suspend fun getMoviesByGenreFromApi(intIdGenre: Int): List<MovieItem>{
        val movieList = service.getMoviesByGenre(intIdGenre)

        return movieList.map { it.toDomain() }
    }

    suspend fun getDetailsMovieFromApi(idMovie: Int): DetailsMovieModel ?{
        val details = service.getDetailsMovie(idMovie)

        return details
    }

    suspend fun insertDetails(details: DetailsMovieEntity){
        detailsMovieDao.insertDetailsMovie(details)
    }

    suspend fun getDetailsMovie(idMovie: Int): DetailsMovieEntity? {
        val details = detailsMovieDao.getDetailsFromMovie(idMovie)
        return details
    }

    suspend fun getAllDetails(): List<DetailsMovieEntity> {
        val details = detailsMovieDao.getAllDetails()
        return details
    }

    suspend fun insertRelMovieGenres(relMovieGenres: RelMovieGenresEntity){
        relMovieGenreDao.insertRelMovieGenre(relMovieGenres)
    }

    suspend fun getMovieGenres(idMovie: Int): List<GenreEntity>?{
        return relMovieGenreDao.getRelMovieGenre(idMovie)
    }

    suspend fun getMoviesByGenreFromDB(idGenre: Int): List<MovieItem>{
        val movieEntity = movieDao.getMoviesByGenre(idGenre) ?: emptyList()

        return movieEntity.map { it.toDomain() }
    }
}