package com.example.movieapp.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.movieapp.data.database.dao.DetailsMovieDao
import com.example.movieapp.data.database.dao.GenreDao
import com.example.movieapp.data.database.dao.MovieDao
import com.example.movieapp.data.database.dao.RelMovieGenreDao
import com.example.movieapp.data.database.entities.DetailsMovieEntity
import com.example.movieapp.data.database.entities.GenreEntity
import com.example.movieapp.data.database.entities.MovieEntity
import com.example.movieapp.data.database.entities.RelMovieGenresEntity

@Database(
    entities = [
        MovieEntity::class
        ,GenreEntity::class
        ,DetailsMovieEntity::class
        ,RelMovieGenresEntity::class
   ]
    , version = 1
)
abstract class DataBaseApp: RoomDatabase() {

    abstract fun getMovieDao(): MovieDao
    abstract fun getGenreDao(): GenreDao
    abstract fun getDetailDao(): DetailsMovieDao
    abstract fun getRelMovieGenreDao(): RelMovieGenreDao
}