package com.example.movieapp.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.movieapp.domain.model.GenreItem

@Entity(tableName ="cat_genres")
data class GenreEntity (
    @PrimaryKey(autoGenerate = false)  @ColumnInfo(name = "id") val id: Int
    ,@ColumnInfo(name = "genre") val genre: String
    ,@ColumnInfo(name = "backgroundColor") var backgroundColor: String
    ,@ColumnInfo(name = "type") var type: String
)

fun GenreItem.toDB() = GenreEntity(
    id
    , genre
    , backgroundColor
    , type
)