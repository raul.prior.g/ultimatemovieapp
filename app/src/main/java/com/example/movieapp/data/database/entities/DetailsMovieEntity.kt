package com.example.movieapp.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.movieapp.data.model.DetailsMovieModel
import com.example.movieapp.data.model.GenreModel

@Entity(tableName = "ctrl_details_movie")
data class DetailsMovieEntity (
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id_db: Int= 0
        ,@ColumnInfo(name = "movie_id") val id: Int
        ,@ColumnInfo(name = "homepage") val homepage: String
        ,@ColumnInfo(name = "overview") val overview: String
        ,@ColumnInfo(name = "original_title") val original_title: String
        ,@ColumnInfo(name = "release_date") val release_date: String
        ,@ColumnInfo(name = "status") val status: String
        ,@ColumnInfo(name = "title") val title: String
        ,@ColumnInfo(name = "poster_path") val poster_path: String
        )

fun DetailsMovieModel.toDB() = DetailsMovieEntity(
        id = id
        ,homepage = homepage
        ,overview = overview
        ,original_title = original_title
        ,release_date = release_date
        ,status = status
        ,title = title
        ,poster_path = poster_path
)