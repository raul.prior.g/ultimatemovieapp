package com.example.movieapp.domain.model

import com.example.movieapp.data.database.entities.MovieEntity
import com.example.movieapp.data.model.MovieModel

data class MovieItem (
    var intIdentity: Int
    ,var strOriginalLanguage: String
    ,var strOriginalTitle: String
    ,var dbPopularity: Double
    ,var strPosterPath: String
    ,var strReleaseDate: String
    ,var strTitle: String
    ,var dbVoteAverage: Double
    ,var dbVoteCount: Double
)

fun MovieModel.toDomain() = MovieItem(
    intIdentity
    , strOriginalLanguage
    , strOriginalTitle
    , dbPopularity
    , strPosterPath
    , strReleaseDate
    , strTitle
    , dbVoteAverage
    , dbVoteCount
)

fun MovieEntity.toDomain() = MovieItem(
    intIdentity
    , strOriginalLanguage
    , strOriginalTitle
    , dbPopularity
    , strPosterPath
    , strReleaseDate
    , strTitle
    , dbVoteAverage
    , dbVoteCount
)