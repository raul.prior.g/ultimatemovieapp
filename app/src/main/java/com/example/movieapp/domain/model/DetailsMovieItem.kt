package com.example.movieapp.domain.model

import com.example.movieapp.data.database.entities.DetailsMovieEntity
import com.example.movieapp.data.model.DetailsMovieModel
import com.example.movieapp.data.model.GenreModel
import javax.inject.Inject

data class DetailsMovieItem @Inject constructor(
    val id: Int
    ,val homepage: String
    ,val overview: String
    ,val original_title: String
    ,val release_date: String
    ,val status: String
    ,val title: String
    ,val poster_path: String
)

fun DetailsMovieModel.toDomain() = DetailsMovieItem(
    id
    , homepage
    , overview
    , original_title
    , release_date
    , status
    , title
    , poster_path
)

fun DetailsMovieEntity.toDomain() = DetailsMovieItem(
    id
    , homepage
    , overview
    , original_title
    , release_date
    , status
    , title
    , poster_path
)