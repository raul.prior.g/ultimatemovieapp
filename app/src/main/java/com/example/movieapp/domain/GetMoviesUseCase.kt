package com.example.movieapp.domain

import com.example.movieapp.data.MoviesRepository
import com.example.movieapp.data.database.entities.toDataBase
import com.example.movieapp.domain.model.MovieItem
import javax.inject.Inject

class GetMoviesUseCase @Inject constructor(
    private val repository: MoviesRepository
) {
    suspend fun checkFromBDFirst(query:String): List<MovieItem>{
        var movies = repository.getAllMoviesFromDataBase()

        if(movies.isEmpty()){
            movies = repository.getAllMoviesFromApi(query)

            if (movies.isNotEmpty()){
                repository.insertMovies(movies.map { it.toDataBase() })
            }
        }
        return movies
    }

    suspend fun checkFromApiFirst(query:String): List<MovieItem>{
        var movies = repository.getAllMoviesFromApi(query)

        if(movies.isEmpty()){
            movies = repository.getAllMoviesFromDataBase()
        }else{
            repository.deleteAllMovies()
            repository.insertMovies(movies.map { it.toDataBase() })
        }
        return movies
    }
}