package com.example.movieapp.domain.model

import com.example.movieapp.data.database.entities.GenreEntity
import com.example.movieapp.data.model.GenreModel
import javax.inject.Inject

data class GenreItem @Inject constructor (
    var id: Int
    ,var genre: String
    ,var backgroundColor: String
    ,var type: String
    )

fun GenreEntity.toDomain() = GenreItem(
    id
    , genre
    , backgroundColor
    , type
)

fun GenreModel.toDomain() = GenreItem(
    id
    , genre
    , backgroundColor ?: ""
    , type ?: ""
)