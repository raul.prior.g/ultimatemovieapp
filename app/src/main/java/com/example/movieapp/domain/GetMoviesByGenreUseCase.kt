package com.example.movieapp.domain

import com.example.movieapp.data.MoviesRepository
import com.example.movieapp.data.database.entities.toDataBase
import com.example.movieapp.domain.model.MovieItem
import javax.inject.Inject

class GetMoviesByGenreUseCase @Inject constructor(
    private val repository: MoviesRepository
    ) {

    suspend operator fun invoke(intIdGenre: Int): List<MovieItem>{
        val moviesFromAPI = repository.getMoviesByGenreFromApi(intIdGenre)

        if(moviesFromAPI.isEmpty()){
            val moviesFromDB = repository.getMoviesByGenreFromDB(intIdGenre)
            return moviesFromDB
        }else{
            repository.insertMovies(moviesFromAPI.map { it.toDataBase() })
            return moviesFromAPI
        }
    }

}