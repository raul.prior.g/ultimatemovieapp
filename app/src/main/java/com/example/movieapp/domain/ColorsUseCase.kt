package com.example.movieapp.domain

import com.example.movieapp.data.ColorsRepository
import javax.inject.Inject

class ColorsUseCase @Inject constructor(
    private val repository: ColorsRepository
){
    suspend operator fun invoke(intNoColors: Int): List<String> = repository.getRandomColors(intNoColors)
}