package com.example.movieapp.domain

import com.example.movieapp.data.MoviesRepository
import com.example.movieapp.data.database.entities.RelMovieGenresEntity
import com.example.movieapp.data.database.entities.toDB
import com.example.movieapp.domain.model.DetailsMovieItem
import com.example.movieapp.domain.model.GenreItem
import com.example.movieapp.domain.model.toDomain
import javax.inject.Inject

class GetDetailsMovieUseCase @Inject constructor(
    private val repository: MoviesRepository
) {
    suspend fun getDetails(idMovie: Int): DetailsMovieItem? {
        val detailsDB = repository.getDetailsMovie(idMovie)

        if(detailsDB == null){
            val details = repository.getDetailsMovieFromApi(idMovie)

            if(details == null) {
                return null
            }

            //Guardar en Base de datos
            repository.insertDetails(details.toDB())

            details.genres.map {
                repository.insertRelMovieGenres(RelMovieGenresEntity(0, details.id, it.id))
            }

            return details.toDomain()
        }

        return detailsDB.toDomain()
    }

    suspend fun getMovieGenres(idMovie: Int): List<GenreItem>{
        val response = repository.getMovieGenres(idMovie) ?: emptyList()

        return response.map { it.toDomain() }
    }
}