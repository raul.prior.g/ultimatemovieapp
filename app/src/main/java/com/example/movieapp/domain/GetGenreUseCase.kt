package com.example.movieapp.domain

import android.util.Log
import com.example.movieapp.data.GenresRepository
import com.example.movieapp.data.database.entities.toDB
import com.example.movieapp.data.model.GenreModel
import com.example.movieapp.domain.model.GenreItem
import javax.inject.Inject

class GetGenreUseCase @Inject constructor(
    private val repository: GenresRepository
    ,private val getColorsUseCase: ColorsUseCase
) {

    suspend operator fun invoke(): List<GenreItem>{
        var genres: List<GenreItem> = repository.getAllGenresByDB()

        if(genres.isEmpty()){
            genres = repository.getAllGenresByApi()

            genres = fillColors(genres)

            if(genres.isNotEmpty()){
                repository.deleteAllGenres()
                repository.insertGenres(genres.map { it.toDB() })
            }
        }

        return genres
    }

    private suspend fun fillColors(genres: List<GenreItem>): List<GenreItem>{
        if(genres.isNotEmpty()){
            val colors = getColorsUseCase.invoke(genres.size)

            if (colors.isEmpty()){
                Log.e("TAG", "GetGenreUseCase: --> Error al obtener los colores"  )
            }else{
                for(i in 0..genres.size-1){
                    genres[i].backgroundColor = colors[i]
                    genres[i].type = "movie"
                }
            }
        }

        return genres
    }
}